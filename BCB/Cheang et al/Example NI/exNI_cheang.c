#include "../ex_main.h"

void cheang_sol_nested_ifs(unsigned x) {
    uint8_t val1, val2;
    if (x < array1_size) {
        val1 = array1[x];
        if (val1 & 1) {
            _mm_lfence();
            val2 = array2[0];
        }   
    }
}