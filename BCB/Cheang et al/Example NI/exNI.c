#include "../ex_main.h"

void victim_function_nested_ifs(unsigned x) {
    uint8_t val1, val2;
    if (x < array1_size) {
        val1 = array1[x];
        if (val1 & 1) {
            val2 = array2[0];
        }   
    }
}