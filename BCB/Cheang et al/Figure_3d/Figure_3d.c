#include "../ex_main.h"

__declspec(dllexport)
uint8_t foo(unsigned i) {
    if (i < N) {
        uint8_t v = array1[i];
        _mm_lfence();
        return array2[v*S];
        }
    return 0;
}