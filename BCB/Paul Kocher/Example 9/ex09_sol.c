#include "../ex_main.h"

__declspec(dllexport)
void solution_v09(size_t x, int *x_is_safe) {
    if (*x_is_safe) {
        uint8_t v = array1[x] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
}
