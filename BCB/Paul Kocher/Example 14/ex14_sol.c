#include "../ex_main.h"

__declspec(dllexport)
void solution_v14(size_t x) {
    if (x < array1_size) {
        uint8_t v = array1[x ^ 255] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
}
