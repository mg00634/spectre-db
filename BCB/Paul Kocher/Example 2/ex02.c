#include "../ex_main.h"

__forceinline void leakByteLocalFunction_v02(uint8_t k) {
    temp &= array2[(k)* 512];
}

__declspec(dllexport)
void victim_function_v02(size_t x) {
    if (x < array1_size) {
        leakByteLocalFunction_v02(array1[x]);
    }
}
