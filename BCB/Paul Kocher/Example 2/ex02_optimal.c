#include "../ex_main.h"

__forceinline void leakByteLocalFunction_v02(uint8_t k) {
    uint8_t v = k * 512;
    _mm_lfence();
    temp &= array2[v];
}

__declspec(dllexport)
void optimal_solution_v02(size_t x) {
    if (x < array1_size) {
        leakByteLocalFunction_v02(array1[x]);
    }
}
