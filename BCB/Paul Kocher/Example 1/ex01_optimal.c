#include "../ex_main.h"

__declspec(dllexport)
void optimal_solution_v01(size_t x) {
   if (x < array1_size) {
      uint8_t v = array1[x] * 512;
      _mm_lfence();
      temp &= array2[v];
   }
}
