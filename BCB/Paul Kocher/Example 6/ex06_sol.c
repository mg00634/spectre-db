#include "../ex_main.h"

__declspec(dllexport)
void solution_v06(size_t x) {
    if ((x & array_size_mask) == x) {
        uint8_t v = array1[x] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
}
