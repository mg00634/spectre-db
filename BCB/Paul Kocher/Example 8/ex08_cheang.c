#include "../ex_main.h"
#include <stdbool.h>

__declspec(dllexport)
void cheang_sol_v08(size_t x) {
    bool cond = x < array1_size;
    _mm_lfence();
    temp &= array2[array1[cond ? (x + 1) : 0] * 512];
}
