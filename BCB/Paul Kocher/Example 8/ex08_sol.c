#include "../ex_main.h"

__declspec(dllexport)
void optimal_sol_v08(size_t x) {
    uint8_t v = array1[x < array1_size ? (x + 1) : 0] * 512;
    _mm_lfence();
    temp &= array2[v];
}
