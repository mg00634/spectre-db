#include "../ex_main.h"

__forceinline int is_x_safe(size_t x) { if (x < array1_size) return 1; return 0; }

__declspec(dllexport)
void victim_function_v13(size_t x) {
    if (is_x_safe(x)) {
        uint8_t v = array1[x] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
}
