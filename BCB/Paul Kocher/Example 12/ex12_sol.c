#include "../ex_main.h"

__declspec(dllexport)
void optimal_sol_v12(size_t x, size_t y) {
    if ((x + y) < array1_size) {
        uint8_t v = array1[x + y] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
}
