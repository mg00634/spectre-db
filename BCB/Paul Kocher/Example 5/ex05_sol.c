#include "../ex_main.h"

__declspec(dllexport)
void optimal_solution_v05(size_t x) {
    size_t i;
    if (x < array1_size) {
        for (i = x - 1; i >= 0; i--) {
            uint8_t v = array1[i] * 512;
            _mm_lfence();
            temp &= array2[v];
        }
    }
}
