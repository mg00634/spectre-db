#include "../ex_main.h"

__declspec(dllexport)
void victim_function_v05(size_t x) {
    size_t i;
    if (x < array1_size) {
        for (i = x - 1; i >= 0; i--)
            temp &= array2[array1[i] * 512];
    }
}
