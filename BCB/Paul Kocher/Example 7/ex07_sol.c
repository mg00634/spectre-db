#include "../ex_main.h"

__declspec(dllexport)
void optimal_sol_v07(size_t x) {
    static size_t last_x = 0;
    if (x == last_x) {
        uint8_t v = array1[x] * 512;
        _mm_lfence();
        temp &= array2[v];
    }
    if (x < array1_size)
        last_x = x;
}
