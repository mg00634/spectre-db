#include "../ex_main.h"

__declspec(dllexport)
void optimal_sol_v07(size_t x) {
    static size_t last_x = 0;
    if (x == last_x) {
        _mm_lfence();
        temp &= array2[array1[x] * 512];
    }
    if (x < array1_size)
        last_x = x;
}
