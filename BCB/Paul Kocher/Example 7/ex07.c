#include "../ex_main.h"

__declspec(dllexport)
void victim_function_v07(size_t x) {
    static size_t last_x = 0;
    if (x == last_x)
        temp &= array2[array1[x] * 512];
    if (x < array1_size)
        last_x = x;
}
