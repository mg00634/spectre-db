#include "../ex_main.h"

__declspec(noinline) void leakByteNoinlineFunction(uint8_t k) { 
    uint8_t v = (k)* 512;
    _mm_lfence();
    temp &= array2[v];
}

__declspec(dllexport)
void solution_v03(size_t x) {
    if (x < array1_size)
        leakByteNoinlineFunction(array1[x]);
}
