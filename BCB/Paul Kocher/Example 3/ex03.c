#include "../ex_main.h"

__declspec(noinline) void leakByteNoinlineFunction(uint8_t k) {
    temp &= array2[(k)* 512]; 
}

__declspec(dllexport)
void victim_function_v03(size_t x) {
    if (x < array1_size)
        leakByteNoinlineFunction(array1[x]);
}
