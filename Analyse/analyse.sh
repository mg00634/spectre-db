#!/bin/bash

eval $(opam env)

invoke_bap() {
    local path=$1

    bap "$path.dll" -dbir --optimization-level=3 --bil-enable-intrinsics=llvm:lfence > "$path.bir"
    bap "$path.dll" -dbil --optimization-level=3 --bil-enable-intrinsics=llvm:lfence > "$path.bil"
    bap "$path.dll" -dbil.adt --optimization-level=3 --bil-enable-intrinsics=llvm:lfence > "$path.bil.adt"
}


bcb_path="/BCB"

for i in {1..15}; do
    filename="$i"
    if [ condition ]; then
        filename="0$i"
    fi
    invoke_bap "$bcb_path/Paul Kocher/Example $i/mitigated/ex$filename"
    invoke_bap "$bcb_path/Paul Kocher/Example $i/unmitigated/ex$filename"
done

cheang_path="$bcb_path/Cheang et al"

invoke_bap "$cheang_path/Example CV/exCV"
invoke_bap "$cheang_path/Example NI/exNI"
invoke_bap "$cheang_path/Figure_3a/Figure_3a"
invoke_bap "$cheang_path/Figure_3b/Figure_3b"
invoke_bap "$cheang_path/Figure_3c/Figure_3c"
invoke_bap "$cheang_path/Figure_3d/Figure_3d"
