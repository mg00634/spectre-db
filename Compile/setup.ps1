#Requires -RunAsAdministrator
################################################################################
# setup.ps1
################################################################################

# Test if a command exists (TRUE for yes, FALSE for no)
function Test-Command {
    param ($Command)

    try {
        Get-Command $Command
        return $true;
    }
    catch [System.Management.Automation.CommandNotFoundException] { return $false; }
}

# Check chocolately is installed, if not attempt to install
function Get-Choco {
    if (!(Test-Command choco)) {
        Set-ExecutionPolicy Bypass -Scope Process -Force;
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
        Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'));
    }
}

# Get the specified dependency or install it with chocolatey, testing it is installed with $DependencyAlias
function Get-Alias-Dependency {
    param ($DependencyAlias, $Dependency)

    if (!(Test-Command $DependencyAlias)) {
        choco install -y $Dependency
    }
}

# Get the specified dependency or install it with chocolatey
function Get-Dependency {
    param ($Dependency)

    return Get-Alias-Dependency $Dependency $Dependency;
}

# Compile 
function Invoke-Msvc {
    param (
        [string]$File1,
        [string]$File2 = "",
        [bool]$SpectreMitigations = $false
    )    

    $qspectre = "";
    If ($SpectreMitigations) {
        $qspectre = "/Qspectre";
    }

    & $msvc_compiler /LD /MD /Z7 /FAcs /nologo /W4 /O2 $qspectre `
        /I $msvc_include /I $win_devkit_include `
        $File1 $File2 `
        /link /LIBPATH:$msvc_lib /LIBPATH:$win_um /LIBPATH:$win_ucrt
}

# Install required dependencies using chocolately
Get-Choco
#Get-Dependency git
choco install -y visualstudio2022buildtools;
choco install -y visualstudio2022-workload-vctools --package-parameters '--includeRecommended --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64';
#--add Microsoft.VisualStudio.Component.Windows10SDK.19041
Install-PackageProvider NuGet -Force;
Install-Module VSSetup -Scope CurrentUser -Force;

$vs = (Get-VSSetupInstance).InstallationPath
$msvc = Join-Path $vs "VC/Tools/MSVC/*" -Resolve

If (-Not (Test-Path -Path $msvc)) {
    Write-Error "Verify version of msvc $msvc";
    Exit 1;
}

$msvc_compiler = Join-Path $msvc "bin/Hostx64/x64/cl.exe"
$msvc_include = Join-Path $msvc "include"
$msvc_lib = Join-Path $msvc "lib/x64"

If (-Not (Test-Command $msvc_compiler)) {
    Write-Error "Verify msvc compiler exists $msvc_compiler";
    Exit 1;
}

If (-Not (Test-Path -Path $msvc_include)) {
    Write-Error "Verify msvc include dir exists $msvc_include";
    Exit 1;
}

If (-Not (Test-Path -Path $msvc_lib)) {
    Write-Error "Verify msvc lib exists $msvc_lib";
    Exit 1;
}

$win_devkit = 'C:\Program Files (x86)\Windows Kits\10\'
$win_devkit_include = Join-Path $win_devkit 'Include\10.0.22621.0\ucrt'
$win_devkit_lib_root = Join-Path $win_devkit 'Lib\10.0.22621.0'
$win_um = Join-Path $win_devkit_lib_root 'um/x64'
$win_ucrt = Join-Path $win_devkit_lib_root 'ucrt/x64'

###########################
# Kocher Examples

# without mitigations
for ($i = 1; $i -le 15; $i++) {
  $fileName = $i.ToString()
  If ($i -lt 10) {
    $fileName = '0' + $i.ToString()
  }

  Invoke-Msvc ".\BCB\Paul Kocher\Example $i\ex$fileName.c" '.\BCB\Paul Kocher\ex_main.c'
  New-Item -ItemType Directory -Force -Path "C:\msvc\BCB\Paul Kocher\Example $i\unmitigated"
  Move-Item -Path "C:\ex*" -Destination "C:\msvc\BCB\Paul Kocher\Example $i\unmitigated"
}

# with mitigations
for ($i = 1; $i -le 15; $i++) {
  $fileName = $i.ToString()
  If ($i -lt 10) {
    $fileName = '0' + $i.ToString()
  }

  Invoke-Msvc ".\BCB\Paul Kocher\Example $i\ex$fileName.c" '.\BCB\Paul Kocher\ex_main.c' -SpectreMitigations $true
  New-Item -ItemType Directory -Force -Path "C:\msvc\BCB\Paul Kocher\Example $i\mitigated"
  Move-Item -Path "C:\ex*" -Destination "C:\msvc\BCB\Paul Kocher\Example $i\mitigated"
}

###########################
# Cheang et al Examples

$cheangs = @{
    'Example CV' = 'exCV'
    'Example NI' = 'exNI'
    'Figure_3a' = 'Figure_3a'
    'Figure_3b' = 'Figure_3b'
    'Figure_3c' = 'Figure_3c'
    'Figure_3d' = 'Figure_3d'
}

foreach ($example in $cheangs.Keys) {
  $filename = $cheangs[$example];
  Invoke-Msvc ".\BCB\Cheang et al\$example\$filename.c" '.\BCB\Cheang et al\ex_main.c' -SpectreMitigations $true
  New-Item -ItemType Directory -Force -Path "C:\msvc\BCB\Cheang et al\$example"
  Move-Item -Path "C:\ex*" -Destination "C:\msvc\BCB\Cheang et al\$example"
  Move-Item -Path "C:\$filename*" -Destination "C:\msvc\BCB\Cheang et al\$example"
}


#& $msvc_compiler /Bv 2>&1 > msvc-version.txt